FROM node:14 AS ui
WORKDIR /usr/app/ui
COPY ./ui/package*.json ./
RUN npm install
COPY ./ui/public/ ./public/
COPY ./ui/*.js ./
COPY ./ui/src/ ./src/
ENV NODE_ENV production
RUN npm run build

FROM node:14
WORKDIR /usr/app
COPY package*.json ./
RUN mkdir -p /usr/app/ui/dist && npm install
COPY ./server.mjs .
COPY ./config/ .
COPY --from=ui /usr/app/ui/dist/ ./ui/dist/

EXPOSE 8181
CMD [ "node", "server.mjs" ]
