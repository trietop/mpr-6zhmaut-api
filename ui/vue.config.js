/* jshint node: true */

process.env.VUE_APP_API = '//music.trietop.org/mpr';

module.exports = {
  "publicPath": process.env.NODE_ENV === 'production' ?
    '/mpr/ui/' : '/',
  "assetsDir": './',
  "transpileDependencies": [
    "vuetify"
  ]
};