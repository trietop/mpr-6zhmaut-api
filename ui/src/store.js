/* jshint node: true */

import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import VueAxios from 'vue-axios';
import ENUM from './enums';

Vue.use(Vuex);
Vue.use(VueAxios, axios);

const API_URL = process.env.VUE_APP_API;
console.log("Using API URL: " + API_URL);

// root state object.
// each Vuex instance is just a single state tree.
const state = {
  lifecycle: ENUM.INIT,
  showSettings: false,
  layout: {},
  zones: {},
  groups: {},
  socket: {
    isConnected: false,
    message: '',
    reconnectError: false,
  }
};

// mutations are operations that actually mutate the state.
// each mutation handler gets the entire state tree as the
// first argument, followed by additional payload arguments.
// mutations must be synchronous and can be recorded by plugins
// for debugging purposes.
const mutations = {
  SET_LIFECYCLE(state, val) {
    state.lifecycle = val;
  },
  SET_SHOW_SETTINGS(state, val) {
    state.showSettings = val;
  },
  SET_LAYOUT(state, layout) {
    state.layout = layout;
  },
  SET_ZONES(state, zones) {
    state.zones = zones;
  },
  SET_ZONE(state, params) {
    var z = state.zones[params.zone];
    Vue.set(z, params.setting, params.data[params.zone][params.setting]);
  },
  UPDATE_ZONES(state, zones) {
    Object.entries(zones).map(z => Vue.set(state.zones, z[0], {
      ...z[1]
    }));
  },
  ADD_ZONE_TO_GROUPS(state, params) {
    for (var group of params.groups) {
      if (!(group in state.groups)) {
        state.groups[group] = [];
      }
      if (!state.groups[group].includes(params.zone)) {
        state.groups[group].push(params.zone);
      }
    }
  },
  SOCKET_ONOPEN(state) {
    //Vue.prototype.$socket = event.currentTarget;
    state.socket.isConnected = true;
  },
  SOCKET_ONCLOSE(state) {
    state.socket.isConnected = false;
  },
  SOCKET_ONERROR() {
    console.error("error");
  },
  // default handler called for all methods
  SOCKET_ONMESSAGE(state, event) {
    state.socket.message = event.data;
    var zones = JSON.parse(event.data);
    Object.entries(zones).map(z => Vue.set(state.zones, z[0], {
      ...z[1]
    }));
  },
  // mutations for reconnect methods
  SOCKET_RECONNECT() {
    console.info("reconnectting...");
  },
  SOCKET_RECONNECT_ERROR(state) {
    state.socket.reconnectError = true;
  }
};

// actions are functions that cause side effects and can involve
// asynchronous operations.
const actions = {
  loaded: ({
    commit
  }) => commit('SET_LOADING', false),
  async load({
    dispatch,
    commit
  }) {
    commit('SET_LIFECYCLE', ENUM.LOADING);
    await dispatch("loadLayout");
    await dispatch("loadZones");
    commit('SET_LIFECYCLE', ENUM.LOADED);
  },
  async loadLayout({
    commit
  }) {
    return axios
      .get(API_URL + '/config/layout.json')
      .then(r => r.data)
      .then(layout => {
        commit('SET_LAYOUT', layout);
      });
  },
  async loadZones({
    commit
  }) {
    return axios
      .get(API_URL + '/zones')
      .then(r => r.data)
      .then(zones => {
        commit('SET_ZONES', zones);
      });
  },
  async reloadZones({
    commit
  }) {
    return axios
      .get(API_URL + '/refresh')
      .then(r => r.data)
      .then(zones => {
        commit('UPDATE_ZONES', zones);
      });
  },
  async setting({
    commit
  }, conf) {
    return axios
      .post(API_URL + '/zones/' + conf.zone + "/" + conf.setting, conf.value, {
        headers: {
          "Content-Type": "text/plain",
          "Accept": "application/json"
        }
      })
      .then(r => r.data)
      .then(obj => {
        if (conf.group) {
          commit('UPDATE_ZONES', obj);
        } else {
          commit('SET_ZONE', {
            zone: conf.zone,
            setting: conf.setting,
            data: obj
          });
        }
      });
  },
  addToGroup({
    commit
  }, conf) {
    commit("ADD_ZONE_TO_GROUPS", conf);
  },
  setShowSettings({
    commit
  }, val) {
    commit('SET_SHOW_SETTINGS', val);
  }
};

// getters are functions.
const getters = {
  getAmps: ({
    zones
  }) => {
    return Object.entries(zones).map(z => z[1].amp).filter((z, i, a) => a.indexOf(z) === i);
  },
  getZonesByAmp: ({
    zones
  }) => (id) => {
    return Object.entries(zones).filter(zone => zone[1].amp === id).map(ent => ent[1]);
  },
  getZonesByGroup: (state, getters) => (id) => {
    return getters.getZonesByIds(state.groups[id]);
  },
  getZoneById: ({
    zones
  }) => (id) => {
    return zones[id];
  },
  getZonesByIds: ({
    zones
  }) => (ids) => {
    return Object.entries(zones).filter(zone => ids.includes(zone[1].zone)).map(ent => ent[1]);
  },
  getAmpConfigById: ({
    layout
  }) => (id) => {
    return layout.amps.find(s => s.id === id);
  },
  getZoneConfigById: ({
    layout
  }) => (id) => {
    return layout.zones.find(s => s.id === id);
  },
  getSource: ({
    layout
  }) => (id) => {
    return layout.sources.find(s => s.value === id);
  },
  getSources: ({
    layout
  }) => {
    return layout.sources;
  },
  getGroups: ({
    layout
  }) => {
    return layout.groups;
  },
  getShowSettings: ({
    showSettings
  }) => {
    return showSettings;
  },
  getGroupMembers: ({
    groups
  }) => (grp) => {
    return groups[grp];
  }
};

// A Vuex instance is created by combining the state, mutations, actions,
// and getters.
export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
});