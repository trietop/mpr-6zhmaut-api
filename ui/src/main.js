import Vue from 'vue';
import VueNativeSock from 'vue-native-websocket';

import store from './store';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import './registerServiceWorker';

const API_URL = process.env.VUE_APP_API;

Vue.config.productionTip = false;
Vue.use(VueNativeSock, 'wss:' + API_URL + '/events', {
  store: store,
  reconnection: true, // (Boolean) whether to reconnect automatically (false)
  reconnectionAttempts: 5, // (Number) number of reconnection attempts before giving up (Infinity),
  reconnectionDelay: 3000, // (Number) how long to initially wait before attempting a new (1000)
});

new Vue({
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app');