/* jshint node: true */

import rxjsOp from 'rxjs/operators/index.js';
import rxjs from 'rxjs';
import compression from 'compression';
import express from "express";
import morgan from "morgan";
import bodyParser from "body-parser";
import log from "log";

import SerialPort from "serialport";
import Readline from '@serialport/parser-readline';

import logger from "log-node";
import expressWs from 'express-ws';

process.on('SIGTERM', function () {
  process.exit(0);
});

logger();

const { fromEvent } = rxjs;
const { filter, take, map } = rxjsOp;

const appWs = expressWs(express());
const app = appWs.app;

const wss = appWs.getWss('/mpr/events');

var logFormat = "[:date[iso]] - :remote-addr - :method :url :status :response-time ms - :res[content-length]b";
app.use(morgan(logFormat));
app.use(bodyParser.text({
  type: '*/*'
}));
app.use(compression());

const ReQuery = /^true$/i.test(process.env.REQUERY);
const UseCORS = /^true$/i.test(process.env.CORS);
const AmpCount = process.env.AMPCOUNT || 1;
const ZoneCount = 6 * AmpCount;
const BaudRate = parseInt(process.env.BAUDRATE || 9600);

var device = process.env.DEVICE || "/dev/ttyUSB0";
var connection = new SerialPort(device, {
  baudRate: BaudRate,
});

const queryControllers = () => {
  for (let i = 1; i <= AmpCount; i++) {
    connection.write("?" + i.toString() + "0\r");
  }
};

const parser = connection.pipe(new Readline({
  delimiter: "\n",
  encoding: "ascii"
}));

const zoneData = (data) => {
  var zone = data.toString("ascii").match(/#>(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/);
  if (zone != null) {
    return {
      "zone": zone[1],
      "amp": zone[1][0] + "0",
      "pa": zone[2],
      "power": zone[3],
      "mute": zone[4],
      "dt": zone[5],
      "volume": zone[6],
      "treble": zone[7],
      "bass": zone[8],
      "balance": zone[9],
      "source": zone[10],
      "ls": zone[11]
    };
  }
  return null;
};

const zoneOb = fromEvent(parser, 'data')
  .pipe(filter(e => e != null && e.startsWith("#>")),
    take(ZoneCount),
    map(zoneData));

var lastState = null;

function loadState() {
  let promise = new Promise(function (resolve, reject) {
    var zo = {};
    const subscrip = zoneOb.subscribe(
      x => zo[x.zone] = x,
      err => reject(err),
      () => {
        subscrip.unsubscribe();
        lastState = zo;
        resolve(zo);
      }
    );
    queryControllers();
  });
  return promise;
}

function sendData(obj) {
  const msg = JSON.stringify(obj);
  wss.clients.forEach(client => client.send(msg));
}

app.ws('/mpr/events', function (_ws, _req, next) {
  next();
});

if (UseCORS) {
  app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', req.get('Origin') || '*');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,POST');
    res.header('Access-Control-Expose-Headers', 'Content-Length');
    res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With, Range');
    if (req.method === 'OPTIONS') {
      return res.send(200);
    }
    return next();
  });
}

// Only allow query and control of single zones
app.param('zone', function (req, res, next, zone) {
  if (zone >= 10 && zone <= 36 && zone % 10 <= 6 && Number(zone) !== "NaN") {
    req.zone = zone;
    next();
  } else {
    res.status(500).send({
      error: zone + ' is not a valid zone'
    });
  }
});

app.param('zones', function (req, res, next, attr) {
  var range = attr.split(",");
  req.zones = [];
  for (var zone of range) {
    if (zone >= 10 && zone <= 36 && zone % 10 <= 6 && Number(zone) !== "NaN") {
      req.zones.push(zone);
    } else {
      res.status(500).send({
        error: zone + ' is not a valid zone'
      });
    }
  }
  next();
});

// Validate and standardize control attributes
app.param('attribute', function (req, res, next, attribute) {
  if (typeof attribute !== 'string') {
    res.status(500).send({
      error: attribute + ' is not a valid zone control attribute'
    });
  }
  log.info(`Request body: ${req.body}`);
  var arg = 0;
  if (req.body === "true") {
    arg = 1;
  } else if (!isNaN(req.body)) {
    arg = parseInt(req.body, "10");
  }
  switch (attribute.toLowerCase()) {
    case "pa":
      req.attribute = "pa";
      req.key = "pa";
      req.body = arg > 0 ? "01" : "00";
      next();
      break;
    case "pr":
    case "power":
      req.attribute = "pr";
      req.key = "power";
      req.body = arg > 0 ? "01" : "00";
      next();
      break;
    case "mu":
    case "mute":
      req.attribute = "mu";
      req.key = "mute";
      req.body = arg > 0 ? "01" : "00";
      next();
      break;
    case "dt":
      req.attribute = "dt";
      req.key = "dt";
      req.body = arg > 0 ? "01" : "00";
      next();
      break;
    case "vo":
    case "volume":
      req.attribute = "vo";
      req.key = "volume";
      req.body = arg < 0 ? "00" : (arg > 38 ? "38" : arg.toString().padStart(2, '0'));
      next();
      break;
    case "tr":
    case "treble":
      req.attribute = "tr";
      req.key = "treble";
      req.body = arg < 0 ? "00" : (arg > 14 ? "14" : arg.toString().padStart(2, '0'));
      next();
      break;
    case "bs":
    case "bass":
      req.attribute = "bs";
      req.key = "bass";
      req.body = arg < 0 ? "00" : (arg > 14 ? "14" : arg.toString().padStart(2, '0'));
      next();
      break;
    case "bl":
    case "balance":
      req.attribute = "bl";
      req.key = "balance";
      req.body = arg < 0 ? "00" : (arg > 20 ? "20" : arg.toString().padStart(2, '0'));
      next();
      break;
    case "ch":
    case "channel":
    case "source":
      req.attribute = "ch";
      req.key = "source";
      req.body = arg < 1 ? "01" : (arg > 6 ? "06" : arg.toString().padStart(2, '0'));
      next();
      break;
    case "ls":
    case "keypad":
      req.attribute = "ls";
      req.key = "ls";
      req.body = arg > 0 ? "01" : "00";
      next();
      break;
    default:
      res.status(500).send({
        error: attribute + ' is not a valid zone control attribute'
      });
  }
});

app.get('/', function (_req, res) {
  res.redirect('/mpr/ui/index.html');
});

app.get('/mpr', function (_req, res) {
  res.redirect('/mpr/ui/index.html');
});

app.use('/mpr/ui', express.static('ui/dist'));

app.use("/mpr/config", express.static('config'));

app.get('/mpr/refresh', async function (_req, res) {
  const zones = await loadState();
  sendData(zones);
  res.json(zones);
});

app.get('/mpr/zones', async function (_req, res) {
  let zones = lastState;
  if (zones == null) {
    zones = await loadState();
  }
  res.json(zones);
});

app.get('/mpr/zones/:zone', async function (req, res) {
  let zones = lastState;
  if (zones == null) {
    zones = await loadState();
  }
  res.json(zones[req.zone]);
});

app.get('/mpr/zones/:zone/:attribute', async function (req, res) {
  let zones = lastState;
  if (zones == null) {
    zones = await loadState();
  }
  res.json(zones[req.zone][req.key]);
});

app.post('/mpr/zones/:zones/:attribute', async function (req, res) {
  const writeAttribute = () => {
    for (var zone of req.zones) {
      log.info(`Zone ${zone} -> ${req.attribute} = ${req.body}`);
      connection.write("<" + zone + req.attribute + req.body + "\r");
    }
  };
  writeAttribute();
  const zones = await loadState();
  sendData(zones);
  res.json(zones);
});

connection.on("open", () => app.listen(process.env.PORT || 8181));